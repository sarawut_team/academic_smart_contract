const path = require('path');
const fs = require('fs');
const solc = require('solc');

const AcademicPath = path.resolve(__dirname, 'contracts', 'AcademicSystem.sol');
const source = fs.readFileSync(AcademicPath, 'utf8');

module.exports = solc.compile(source, 1).contracts[':AcademicSystem'];
