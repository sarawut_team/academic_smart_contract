pragma solidity ^0.4.18;

contract Owned {
    address owner;
    
    function Owned() public {
        owner = msg.sender;
    }
    
    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }
}

contract AcademicSystem is Owned {
    struct Student {
        uint id;
        string wallet;
        string fullName;
        string department;
        string faculty;
        string subject1;
        string subject2;
        string subject3;
        string subject4;
    }

    mapping (uint => Student) students;
    uint[] public studentIds;
    
    mapping (address => Student) studentsAddr;
    address[] public studentAccounts;
    
    mapping (string => Student) stdWallet;
    string[] public studentWallet;
 
    event studentInfo(
        uint id,
        string wallet,
        string fullName,
        string department,
        string faculty
    );

    event gradeInfo(
        string subject1,
        string subject2,
        string subject3,
        string subject4
    );

    function setStudent(address _address, uint _id, string _wallet, string _fullName, string _department, string _faculty, string _subject1, string _subject2, string _subject3, string _subject4) onlyOwner public {
        var student = students[_id];
        student.id = _id;
        student.wallet = _wallet;
        student.fullName = _fullName;
        student.department = _department;
        student.faculty = _faculty;
        student.subject1 = _subject1;
        student.subject2 = _subject2;
        student.subject3 = _subject3;
        student.subject4 = _subject4;
        
        stdWallet[_wallet] = student;
        
        studentIds.push(_id) - 1;
        studentAccounts.push(_address) -1;
        studentWallet.push(_wallet) - 1;
        studentInfo(_id, _wallet, _fullName, _department, _faculty);
        gradeInfo(_subject1, _subject2, _subject3, _subject4);
    }

    function getStudentInfo(uint _id) view public returns (uint, string, string, string, string) {
        return (
            students[_id].id, 
            students[_id].wallet,
            students[_id].fullName, 
            students[_id].department, 
            students[_id].faculty
        );
    }

    function getStudentGrade(uint _id) view public returns (string, string, string, string) {
        return (
            students[_id].subject1, 
            students[_id].subject2,
            students[_id].subject3,
            students[_id].subject4
        );
    }
    
    function getSTDinfobyWallet(string _wallet) view public returns (uint, string, string, string, string) {
        return (
            stdWallet[_wallet].id,
            stdWallet[_wallet].wallet,
            stdWallet[_wallet].fullName, 
            stdWallet[_wallet].department, 
            stdWallet[_wallet].faculty
            );
    }
    
    function getSTDGradebyWallet(string _wallet) view public returns (string, string, string, string) {
        return (
            stdWallet[_wallet].subject1,
            stdWallet[_wallet].subject2,
            stdWallet[_wallet].subject3, 
            stdWallet[_wallet].subject4
            );
    }
    
    function getStudents() view public returns (uint[]) {
        return studentIds;
    }

    function countStudents() view public returns (uint) {
        return studentAccounts.length;

    }

}